<?php

class CartController
{

    public function actionAdd($id){
        $id = intval($id);

        //Добавляем товар в корзину
        $_SESSION['product'] = Cart::addProduct($id);

        if($_SESSION['product']){

            $keyProduct = array_keys($_SESSION['product']);

            //Возвращает общую сумму
            $_SESSION['total_sum'] = Product::getProduсtsByIds($keyProduct);
            //Возвращает общее кол-во
            $_SESSION['total_quantity'] = Cart::getTotalQuantity($_SESSION['product']);
        }

        //Возвращаем пользователя на главную страницу
        header("Location: /");
        return true;
    }


    public function actionIndex(){

        $categorys = Category::getCategory();

        $name = '';
        $phone = '';
        $address = '';

        if (isset($_POST['order'])) {

            $name = htmlspecialchars(trim($_POST['name']));
            $phone = htmlspecialchars(trim($_POST['phone']));
            $address = htmlspecialchars(trim($_POST['address']));

            $errors = false;

            if (Cart::check_length($name, 2, 10)) {
                $errors[] = "Имя не должно быть меньше 2-х символов";
            }

            if (!Cart::checkPhone($phone)) {
                $errors[] = 'Неправильный телефон';
            }

            if (Cart::check_length($address, 5, 120)) {
                $errors[] = 'Укажите адрес доставки';
            }

            //если нет ошибок
            if ($errors == false) {

                //Сохраняем пользователя с данными и возв-ем его id
                $customer_id = User::customers($name, $phone, $address);

                $quantity ="";

                if(isset($_SESSION['product'])){
                    foreach ($_SESSION['product'] as $key => $value){
                        $quantity .= "( $customer_id, $key, {$value['qty']} ),";//customer_id(заказчик),key(id продукта),$value['qty'](кол-во продуктов)
                    }
                    $quantity = substr( $quantity,0,-1);

                    // Сохраняем заказ в БД
                    $result = Order::orders($quantity);

                    unset($_SESSION['product']);
                    unset($_SESSION['total_sum']);
                    unset($_SESSION['total_quantity']);

                }
            }
        }

        require_once($_SERVER['DOCUMENT_ROOT'] . '/views/cart/index.php');
        return true;

    }


    public function actionDeleteProduct($key){
        $key = intval($key);
        if (isset($_SESSION['product'])) {
            if (array_key_exists($key, $_SESSION['product'])) {
                Cart::deleteKey($key);
            } else {
                header("Location: /cart");;
            }
        }
    }

}