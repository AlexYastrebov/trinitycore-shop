<?php

class AdminCategoryController
{
        public function actionIndex(){

            $categorys = Category::getCategory();

            // Получаем список категорий
            $categoriesList = Category::getCategoriesListAdmin();

            // Если форма отправлена
            if (isset($_POST['submit'])) {

                // Получаем данные из формы
                $options['name'] = htmlspecialchars(trim($_POST['name']));
                $options['sort_order'] = htmlspecialchars(trim($_POST['sort_order']));

                // Флаг ошибок в форме
                $errors = false;

                // Валидация
                if (!isset($options['name']) || empty($options['name'])) {
                    $errors[] = 'Заполните поля';
                }

                // Если ошибок нет
                if ($errors == false) {

                    // Добавляем новый товар
                    $id = Category::createCategory($options);

                    // Если запись добавлена
                    if ($id) {
                        // Возвращаемся  на страницу управлениями категориями
                        header("Location: /admin/category");
                    };
                }
            }
            require_once($_SERVER['DOCUMENT_ROOT'] . '/views/admin_category/index.php');
            return true;
        }
}