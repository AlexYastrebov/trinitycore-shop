<?php

class AdminProductController
{
    public function actionIndex(){

        $categorys = Category::getCategory();

        //получаем список товаров
        $productsList = Product::getProducts();

        // Если форма отправлена
        if (isset($_POST['submit'])) {

            // Получаем данные из формы
            $options['name'] = htmlspecialchars(trim($_POST['name']));
            $options['price'] = htmlspecialchars(trim($_POST['price']));
            $options['category_id'] =htmlspecialchars(trim($_POST['category_id']));
            $options['content'] = htmlspecialchars(trim($_POST['content']));
            $options['anons'] = htmlspecialchars(trim($_POST['anons']));

            // Флаг ошибок в форме
            $errors = false;
            // Валидиция значения
            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заполните поля';
            }

            // Если ошибок нет
            if ($errors == false) {

                // Добавляем новый товар
                $id = Product::createProduct($options);

                // Если запись добавлена
                if ($id) {
                    // Перенаправляем пользователя на страницу управлениями товарами
                    header("Location: /admin/product");
                };
            }
        }
        // Подключаем вид
        require_once($_SERVER['DOCUMENT_ROOT'] . '/views/admin_product/index.php');
        return true;
    }
}