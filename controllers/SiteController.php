<?php

class SiteController
{

    public function actionIndex(){

        // Список категорий
        $categorys = Category::getCategory();

        require_once($_SERVER['DOCUMENT_ROOT'] . '/views/site/index.php');
        return true;
    }

}