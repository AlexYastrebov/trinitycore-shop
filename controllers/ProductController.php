<?php

class ProductController
{
    public function actionIndex($category_id){

        // Список категорий
        $categorys = Category::getCategory();

        //получить продукты по заданой категории ($category_id)
        $products = Product::getProductsByCategoryId($category_id);

        require_once($_SERVER['DOCUMENT_ROOT'] . '/views/product/index.php');
        return true;
    }

    public  function actionView($id){

        // Список категорий
        $categorys = Category::getCategory();

        // Получаем инфомрацию о товаре
        $oneProduct = Product::oneProduct($id);

        require_once($_SERVER['DOCUMENT_ROOT'] . '/views/product/view.php');
        return true;
    }
}