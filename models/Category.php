<?php

class Category
{
    public static function getCategory(){

        //Соединение с БД
        $db = Db::getConnection();

        $result = $db->query('SELECT id, name FROM category');

        $categorys = array();

        $i=0;

        while($row = $result->fetch(PDO::FETCH_ASSOC)){

            $categorys[$i]['id'] = $row['id'];
            $categorys[$i]['name'] = $row['name'];

            $i++;
        }
        return $categorys;
    }

    public static function getCategoriesListAdmin()
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Запрос к БД
        $result = $db->query('SELECT * FROM category ORDER BY sort_order ASC');
        // Получение и возврат результатов
        $categoryList = array();
        $i = 0;
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $categoryList[$i]['sort_order'] = $row['sort_order'];

            $i++;
        }
        return $categoryList;
    }

    public static function createCategory($options){

        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'INSERT INTO category (name, sort_order) VALUES (:name, :sort_order)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':sort_order', $options['sort_order'], PDO::PARAM_INT);
        $result = $result->execute();

        return  $result;
    }
}