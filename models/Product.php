<?php

class Product
{

         static function  getProductsByCategoryId($category_id){

             //Соединение с БД
             $db = Db::getConnection();

             $result = $db->query("SELECT * FROM product WHERE category_id = $category_id");

             $products = array();

            $i=0;
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                $products[$i]['id'] = $row['id'];
                $products[$i]['name'] = $row['name'];
                $products[$i]['anons'] = $row['anons'];
                $products[$i]['price'] = $row['price'];
                $products[$i]['img'] = $row['img'];
               $i++;
            }
            return $products;
         }

    // Возвращает продукт с указанным id
    public static function oneProduct($id){

        // Соединение с БД
        $db = Db::getConnection();

        $result = $db->query("SELECT * FROM product WHERE id = $id");

        $oneProduct  = $result->fetch(PDO::FETCH_ASSOC);

        return $oneProduct;
    }

    //Возвращает общую сумму
    public static function getProduсtsByIds($keyProduct)
    {
        $total_sum = 0;

        $db = Db::getConnection();

        $idsString = implode(',', $keyProduct);

        $result = $db->query("SELECT id, category_id, name, price, img  FROM product WHERE  id IN ($idsString) ");

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $_SESSION['product'][$row['id']]['id'] = $row['id'];
            $_SESSION['product'][$row['id']]['category_id'] = $row['category_id'];
            $_SESSION['product'][$row['id']]['name'] = $row['name'];
            $_SESSION['product'][$row['id']]['price'] = $row['price'];
            $_SESSION['product'][$row['id']]['img'] = $row['img'];
            $total_sum += $_SESSION['product'][$row['id']]['qty'] * $row['price'];
        }
        return $total_sum;
    }

    public static function getProducts(){

        // Соединение с БД
        $db = Db::getConnection();

        $productList = array();

        $result = $db->query("SELECT id, name, price FROM product  ORDER BY id");

        $i = 0;
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            $productList[$i]["id"] = $row["id"];
            $productList[$i]["name"] = $row["name"];
            $productList[$i] ["price"] = $row ["price"];

            $i++;
        }
        return $productList;
    }


    //Добавляет новый товар
    public static function createProduct($options)
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'INSERT INTO product (name, price, category_id, content, anons) VALUES (:name, :price, :category_id, :content, :anons)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);

        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':content', $options['content'], PDO::PARAM_INT);
        $result->bindParam(':anons', $options['anons'], PDO::PARAM_INT);
        return $result->execute();
    }
}