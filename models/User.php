<?php

class User
{

    public static function customers($name, $phone, $address){

        $db = Db::getConnection();

        $sql = "INSERT INTO customers (name,phone,address) VALUE (:name,:phone,:address)";

        //Подготавливает запрос к выполнению и возвращает ассоциированный с этим запросом объек
        $result = $db->prepare($sql);

        //Привязывает параметр запроса к переменной
        $result->bindParam(':name',$name,PDO::PARAM_STR);
        $result->bindParam(':phone',$phone,PDO::PARAM_STR);
        $result->bindParam(':address',$address,PDO::PARAM_STR);

        //Запускает подготовленный запрос на выполнение
        $result->execute();

        //Возвращает ID последней вставленной строки или последовательное значение
        return $db->lastInsertId(PDO::FETCH_ASSOC);

    }

}