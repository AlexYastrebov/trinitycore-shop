<?php

class Order
{

    public static function orders($quantity){

        $db = Db::getConnection();
        $sql = "INSERT INTO orders ( customer_id, product_id, quantity ) VALUE  $quantity ";

        //Подготавливает запрос к выполнению и возвращает ассоциированный с этим запросом объек
        $result = $db->prepare($sql);

        //Привязывает параметр запроса к переменной
        $result->bindParam(':quantity',$quantity, PDO::PARAM_INT);

        //Запускает подготовленный запрос на выполнение
       return $result->execute();
    }

    // возвращает все Заказы
    public static function getOrders(){

        //запрос к БД
        $db = Db::getConnection();

        $ordersList = array();
        $result = $db->query('SELECT c.customer_id, c.name, c.phone, c.address, c.date, o.order_id , o.product_id, o.quantity FROM customers c JOIN orders o ON o.customer_id = c.customer_id');

        $i = 0;
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            $ordersList[$i]["customer_id"] = $row["customer_id"];
            $ordersList[$i]["name"] = $row["name"];
            $ordersList[$i]["phone"] = $row ["phone"];
            $ordersList[$i]["address"] = $row["address"];
            $ordersList[$i]["date"] = $row["date"];
            $ordersList[$i]["order_id"] = $row["order_id"];
            $ordersList[$i]["product_id"] = $row["product_id"];
            $ordersList[$i]["quantity"] = $row["quantity"];
            $i++;
        }

        return  $ordersList;
    }

}