<?php

class Cart
{

    public static function check_length($value = "", $min, $max) {
        $result = (mb_strlen($value) < $min || mb_strlen($value) > $max);

        return $result;
    }


    public static function checkPhone($phone)
    {
        $paternPhone ='/([0-9]{3})-([0-9]{3})-([0-9]{2})-([0-9]{2})/';
        if(preg_match($paternPhone, $phone)){
            return true;
        }else{
            return false;
        }
    }

    //Добавляем товар в корзину
    public static function addProduct($id, $qty = 1){

        if(isset($_SESSION['product'][$id])){
            $_SESSION['product'][$id]['qty']+= $qty;
            return $_SESSION['product'];
        }else{
            $_SESSION['product'][$id]['qty'] = $qty;
            return $_SESSION['product'];
        }
    }



    public static function getTotalQuantity($arr){

        $_SESSION['product'] = $arr;

        $_SESSION['total_quantity'] = 0;

        foreach ($_SESSION['product'] as $key => $value){
            if(isset($value['price'])){
                $_SESSION['total_quantity'] += $value['qty'];

            }else{
                unset($_SESSION['product'][$key]);
                header("Location: /");
                exit();
            }
        }
        return  $_SESSION['total_quantity'];
    }


    public static function deleteKey($key){

        if(isset($_SESSION['product'])){
            $_SESSION['total_quantity'] =  $_SESSION['total_quantity'] - $_SESSION['product'][$key]['qty'];

            $_SESSION['total_sum'] = $_SESSION['total_sum'] - $_SESSION['product'][$key]['qty'] * $_SESSION['product'][$key]['price'];

            unset($_SESSION['product'][$key]);
        }
    }

}