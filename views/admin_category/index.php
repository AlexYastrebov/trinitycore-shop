<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Admin category</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="karkas">
    <header>
        <h1 class="logo"><a href="/">SHOP</a></h1>
    </header>

    <div class="content">

        <nav>
            <?php foreach ( $categorys as $category):?>
                <ul>
                    <h3><li><a href="/category/<?=$category['id']?>"><?=$category['name']?></a></li></h3>
                </ul>
            <?php endforeach; ?>
            <h3><a href="/admin">Админ</a></h3>
        </nav>

        <div class="right-bar">
            <h2>Управление категориями</h2>

            <table class="zakaz-table">
                <tr>
                    <td>ID категории</td>
                    <td>Название категории</td>
                    <td>Порядковый номер</td>

                </tr>
                <?php foreach ($categoriesList as $categoryAdmin):?>
                    <tr>
                        <td><?=$categoryAdmin['id']?></td>
                        <td><?=$categoryAdmin['name']?></td>
                        <td><?=$categoryAdmin['sort_order']?></td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <h2>Добавить категорию</h2>

            <form action="#" method="post">
                <p>Название</p>
                <input type="text" name="name"  value="">
                <p>Порядковый номер</p>
                <input type="text" name="sort_order"  value="">
                <br><br>
                <input type="submit" name="submit"  value="Сохранить">
            </form>

        </div>

    </div>

    <footer>
        <p>Copyright © 2017</p>
    </footer>
</div>

</body>
