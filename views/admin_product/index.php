<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Admin product</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="karkas">
    <header>
        <h1 class="logo"><a href="/">SHOP</a></h1>
    </header>

    <div class="content">

        <nav>
            <?php foreach ( $categorys as $category):?>
                <ul>
                    <h3><li><a href="/category/<?=$category['id']?>"><?=$category['name']?></a></li></h3>
                </ul>
            <?php endforeach; ?>
            <h3><a href="/admin">Админ</a></h3>
        </nav>

        <div class="right-bar">
            <h2>Управление товарами</h2>

            <table class="zakaz-table">
                <tr>
                    <td>ID товара</td>
                    <td>Название товара</td>
                    <td>Цена,грн.</td>
                </tr>
                <?php foreach ($productsList as $products):?>
                    <tr>
                        <td><?=$products['id']?></td>
                        <td><?=$products['name']?></td>
                        <td><?=$products['price']?></td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <h2>Добавить товар</h2>

            <form action="#" method="post" >
                <p>Название товара</p>
                <input type="text" name="name" placeholder="" value="">
                <p>Стоимость, грн</p>
                <input type="text" name="price" placeholder="" value="">
                <p>Категория</p>
                <select name="category_id">
                    <?php if (is_array($categorys)): ?>
                        <?php foreach ($categorys as $category): ?>
                            <option value="<?= $category['id']; ?>">
                                <?= $category['name']; ?>
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <p>Детальное описание</p>
                <textarea name="content"></textarea>
                <p>Краткое описание</p>
                <textarea name="anons"></textarea>
                <br/><br/>
                <input type="submit" name="submit"  value="Сохранить">
            </form>
        </div>
    </div>
    <footer>
        <p>Copyright © 2017</p>
    </footer>
</div>

</body>
