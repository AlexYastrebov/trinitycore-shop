
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Products</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

<div class="karkas">
    <header>
        <h1 class="logo"><a href="/">SHOP</a></h1>
    </header>

    <div class="content">
        <nav>
            <?php foreach ( $categorys as $category):?>
                <ul>
                    <h3><li><a href="/category/<?=$category['id']?>"><?=$category['name']?></a></li></h3>
                </ul>
            <?php endforeach; ?>
            <h3><a href="/admin">Админ</a></h3>
        </nav>

        <div class="right-bar">
            <div class="mod">
                <?php foreach ($products as $product):?>
                    <h2><a href="/product/<?=$product['id']?>"><?=$product['name']?></a></h2>
                    <div class="product"><a href="/product/<?=$product['id']?>"><img src="/images/<?=$product['img']?>" width="60px" alt="" /></a></div>
                    <div class="anons"><?=$product['anons']?></div>
                    <div class="nov"></div>
                    <p>Цена :  <span><?=$product['price']?>грн.</span></p>
                <?php endforeach;?>
            </div>
        </div>
    </div>

    <footer>
        <p>Copyright © 2017</p>
    </footer>
</div>
</body>
