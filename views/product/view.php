<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Product</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

<div class="karkas">
    <header>
        <h1 class="logo"><a href="/">SHOP</a></h1>
    </header>

    <div class="content">
        <nav>
            <?php foreach ( $categorys as $category):?>
                <ul>
                    <h3><li><a href="/category/<?=$category['id']?>"><?=$category['name']?></a></li></h3>
                </ul>
            <?php endforeach; ?>
            <h3><a href="/admin">Админ</a></h3>
        </nav>

        <div class="right-bar">
            <div class="mod">
                <h2><?=$oneProduct['name']?></h2>
                <div class="product"><img src="/images/<?=$oneProduct['img']?>"  alt="" /></div>
                <div class="anons"><?=$oneProduct['anons']?></div>
                <div class="nov"></div>
                <p class="cena">Цена :  <span><?=$oneProduct['price']?>грн.</span></p>
                <p><a class="knopka" href="/cart/add/<?=$oneProduct['id']?>">Добавить в корзину</a></p>
                <p><a class="knopka" href="/cart">Корзина</a></p>
            </div>
            <div><?=$oneProduct['content']?></div>
        </div>
    </div>

    <footer>
        <p>Copyright © 2017</p>
    </footer>
</div>
</body>
