
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Cart</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

<div class="karkas">
    <header>
        <h1 class="logo"><a href="/">SHOP</a></h1>
    </header>

    <div class="content">

        <nav>
            <?php foreach ( $categorys as $category):?>
                <ul>
                    <h3><li><a href="/category/<?=$category['id']?>"><?=$category['name']?></a></li></h3>
                </ul>
            <?php endforeach; ?>
        </nav>

        <div class="right-bar">
            <div class="mod">

                <h2>Оформление заказа</h2>

                <?php if(!empty($_SESSION['product'])): ?>
                <table class="zakaz-table">
                    <form method="post" action="">
                        <tr>
                            <td class="z_top">&nbsp;&nbsp;&nbsp;&nbsp;наименование</td>
                            <td class="z_top" align="center">количество</td>
                            <td class="z_top" align="center">цена</td>
                            <td class="z_top" align="center">сумма</td>
                            <td class="z_top" align="center">&nbsp;</td>
                        </tr>
                        <?php foreach ($_SESSION['product'] as $key => $value):?>
                            <tr>
                                <td class="z_name">
                                    <a href="/product/<?=$value['id']?>"><img src="/images/<?=$value['img']?>" width="40" title="<?=$value['name']?>"/></a>
                                    <a href="/product/<?=$value['id']?>"><?=$value['name']?></a>
                                </td>
                                <td class="z_kol"><?=$value['qty']?></td>
                                <td class="z_price"><?=$value['price']?></td>
                                <td class="z_price"><?=$value['price'] * $value['qty']?></td>
                                <td class="z_del"><a href="/cart/delete/<?=$key?>"><img src="/images/delete.jpg" title="удалить товар из заказа" /></a></td>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                            <td class="z_bot">&nbsp;&nbsp;&nbsp;&nbsp;Итого:</td>
                            <td class="z_bot" colspan="3" align="right"><?=$_SESSION['total_quantity']?> шт &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$_SESSION['total_sum']?> грн.</td>
                        </tr>
                </table>

                <h3>Информация для доставки:</h3>

                    <table class="zakaz-data">
                        <tr>
                            <td class="zakaz-txt"><span>*</span>Имя</td>
                            <td class="zakaz-inpt"> <input type="text" name="name" value="<?=$name?>"/></td>
                            <td class="zakaz-prim">Пример: Алексей  </td>
                        </tr>
                        <tr>
                            <td class="zakaz-txt"><span>*</span>Телефон</td>
                            <td class="zakaz-inpt"><input type="text" name="phone" value="<?=$phone?>"/></td>
                            <td class="zakaz-prim">Пример: 098-930-59-04</td>
                        </tr>
                        <tr>
                            <td class="zakaz-txt"><span>*</span>Адрес доставки</td>
                            <td class="zakaz-inpt"><textarea  name="address" cols="45" rows="3" value="<?=$address?>"></textarea></td>
                            <td class="zakaz-prim">Пример: г. Харьков, пр. Ленина, ул. Петра Великого д.19, кв 51.</td>
                        </tr>
                        <tr>
                            <td class="zakaz-txt">Внимание:</td>
                            <td class="zakaz-txt"><span class="red">*</span> - поля, обязательные для заполнения</td>
                        </tr>
                    </table>

                <br/>
                <input type="submit" name="order"  value="Оформить" />
                </form>
                <?php endif; ?>
                <br>
                <?php if (isset($errors) && is_array($errors)): ?>
                    <h4>Не заполнены обязательные поля:</h4>
                    <div class="error" >
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
            <?php if(!empty($result)):?>
                <div>Заказ оформлен. Мы Вам перезвоним.!</div>
            <?php endif; ?>
            <?php if(empty($_SESSION['product'])):?>
            <h3>Корзина пуста!</h3>
            <a href="/">Вернутся к покупкам</a>
            <?php endif;?>
            </div>
        </div>
    </div>
    <footer>
        <p>Copyright © 2017</p>
    </footer>
</div>
</body>