<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Admin order</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="karkas">
    <header>
        <h1 class="logo"><a href="/">SHOP</a></h1>
    </header>

    <div class="content">

        <nav>
            <?php foreach ( $categorys as $category):?>
                <ul>
                    <h3><li><a href="/category/<?=$category['id']?>"><?=$category['name']?></a></li></h3>
                </ul>
            <?php endforeach; ?>
            <h3><a href="/admin">Админ</a></h3>
        </nav>

        <div class="right-bar">
            <h2>Управление заказами</h2>

            <table class="zakaz-table">
                <tr>
                    <td>ID заказ</td>
                    <td>ID заказчика</td>
                    <td>Имя заказчика</td>
                    <td>Телефон</td>
                    <td>Адрес</td>
                    <td>ID продукта</td>
                    <td>Кол-во</td>
                    <td>Дата</td>
                </tr>
                <?php foreach ($ordersList as $order):?>
                    <tr>
                        <td><?=$order['order_id']?></td>
                        <td><?=$order['customer_id']?></td>
                        <td><?=$order['name']?></td>
                        <td><?=$order['phone']?></td>
                        <td><?=$order['address']?></td>
                        <td><?=$order['product_id']?></td>
                        <td><?=$order['quantity']?></td>
                        <td><?=$order['date']?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>

    <footer>
        <p>Copyright © 2017</p>
    </footer>
</div>

</body>