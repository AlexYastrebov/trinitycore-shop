<?php

return array(
    // Управление заказами:
    'admin/order' => 'adminOrder/index',
    // Управление товарами:
    'admin/product' => 'adminProduct/index',
    // Управление категориями:
    'admin/category' => 'adminCategory/index',
    // Админпанель:
    'admin' => 'admin/index',
    // Корзина:
    'cart/delete/([0-9]+)' => 'cart/deleteProduct/$1',
    'cart/add/([0-9]+)' => 'cart/add/$1',
    'cart' => 'cart/index',
    // Товар:
    'product/([0-9]+)' => 'product/view/$1',
    //Категории
    'category/([0-9]+)' => 'product/index/$1',
    //Главная страница
    '(^$)' => 'site/index',
    'index.php' => 'site/index',

);